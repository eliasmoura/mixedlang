mfPkg.addNative(
{
  "teste": {
    "key": "teste",
    "text": "some text",
    "file": "./client/templates/blog.html",
    "line": 15,
    "template": "blog",
    "ctime": 1396796772199,
    "mtime": 1396796772199
  },
  "noUser-chat": {
    "key": "noUser-chat",
    "text": "You aren't logged in. Please, log in to use all the features of the site.",
    "file": "./client/templates/chatroons.html",
    "line": 43,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "public_rooms": {
    "key": "public_rooms",
    "text": "Public rooms",
    "file": "./client/templates/chatroons.html",
    "line": 62,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1398341669193
  },
  "group_chats": {
    "key": "group_chats",
    "text": "Groups",
    "file": "./client/templates/chatroons.html",
    "line": 78,
    "template": "chatrooms",
    "ctime": 1396794056060,
    "mtime": 1398341669193
  },
  "add-find-group": {
    "key": "add-find-group",
    "text": "Add/Find a chat group",
    "file": "./client/templates/chatroons.html",
    "line": 80,
    "template": "chatrooms",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "manage-group": {
    "key": "manage-group",
    "text": "Group managenment",
    "file": "./client/templates/chatroons.html",
    "line": 94,
    "template": "chatrooms",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "friends_contacts": {
    "key": "friends_contacts",
    "text": "Contacts",
    "file": "./client/templates/chatroons.html",
    "line": 113,
    "template": "chatrooms",
    "ctime": 1396794056060,
    "mtime": 1398341669193
  },
  "users": {
    "key": "users",
    "text": "Users",
    "file": "./client/templates/chatroons.html",
    "line": 195,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "message": {
    "key": "message",
    "text": "Message",
    "file": "./client/templates/user.html",
    "line": 316,
    "template": "user-profile",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "correction": {
    "key": "correction",
    "text": "Correction",
    "file": "./client/templates/chatroons.html",
    "line": 249,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "E": {
    "key": "E",
    "text": "E",
    "file": "./client/templates/chatroons.html",
    "line": 272,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "explanation": {
    "key": "explanation",
    "text": "Explanation",
    "file": "./client/templates/chatroons.html",
    "line": 273,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "A": {
    "key": "A",
    "text": "A",
    "file": "./client/templates/chatroons.html",
    "line": 277,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "word_sentence": {
    "key": "word_sentence",
    "text": "word/expretion",
    "file": "./client/templates/chatroons.html",
    "line": 284,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "alternative": {
    "key": "alternative",
    "text": "Alternative",
    "file": "./client/templates/chatroons.html",
    "line": 279,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "U": {
    "key": "U",
    "text": "U",
    "file": "./client/templates/chatroons.html",
    "line": 283,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "usage": {
    "key": "usage",
    "text": "Usage",
    "file": "./client/templates/chatroons.html",
    "line": 285,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "save-correction": {
    "key": "save-correction",
    "text": "Save correction",
    "file": "./client/templates/chatroons.html",
    "line": 296,
    "template": "chatrooms",
    "ctime": 1397014212330,
    "mtime": 1397014212330
  },
  "find-create": {
    "key": "find-create",
    "text": "Find/Create",
    "file": "./client/templates/group.html",
    "line": 9,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "email": {
    "key": "email",
    "text": "Email",
    "file": "./client/templates/user.html",
    "line": 31,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1398707194597
  },
  "sent": {
    "key": "sent",
    "text": "Sent",
    "file": "./client/templates/email.html",
    "line": 13,
    "template": "emails",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "received": {
    "key": "received",
    "text": "Received",
    "file": "./client/templates/email.html",
    "line": 14,
    "template": "emails",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "send": {
    "key": "send",
    "text": "Send",
    "file": "./client/js/email.js",
    "line": 50,
    "func": "function(e,t)",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "to": {
    "key": "to",
    "text": "To",
    "file": "./client/templates/user.html",
    "line": 293,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "subject": {
    "key": "subject",
    "text": "Subject",
    "file": "./client/templates/email.html",
    "line": 67,
    "template": "emails",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "msg": {
    "key": "msg",
    "text": "Message",
    "file": "./client/templates/user.html",
    "line": 294,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341711355
  },
  "find": {
    "key": "find",
    "text": "Find",
    "file": "./client/js/group.js",
    "line": 20,
    "func": "function(e,t)",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "create": {
    "key": "create",
    "text": "Create",
    "file": "./client/js/group.js",
    "line": 24,
    "func": "function(e,t)",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "details": {
    "key": "details",
    "text": "Details",
    "file": "./client/templates/user.html",
    "line": 164,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "group-info": {
    "key": "group-info",
    "text": "Group information",
    "file": "./client/templates/group.html",
    "line": 72,
    "template": "group_chat_finder",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "type": {
    "key": "type",
    "text": "Type",
    "file": "./client/templates/group.html",
    "line": 92,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "select-one": {
    "key": "select-one",
    "text": "Select one",
    "file": "./client/templates/group.html",
    "line": 94,
    "template": "group_chat_finder",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "open_group": {
    "key": "open_group",
    "text": "Open group",
    "file": "./client/templates/group.html",
    "line": 95,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "part_request": {
    "key": "part_request",
    "text": "Participation request",
    "file": "./client/templates/group.html",
    "line": 96,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "memb_indication": {
    "key": "memb_indication",
    "text": "Members indicatioin",
    "file": "./client/templates/group.html",
    "line": 97,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "groupgenre": {
    "key": "groupgenre",
    "text": "Group genre",
    "file": "./client/templates/group.html",
    "line": 102,
    "template": "group_chat_finder",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "music": {
    "key": "music",
    "text": "Music",
    "file": "./client/templates/sign.html",
    "line": 170,
    "template": "login_form",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "sports": {
    "key": "sports",
    "text": "Sports",
    "file": "./client/templates/sign.html",
    "line": 172,
    "template": "login_form",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "literature": {
    "key": "literature",
    "text": "Literature",
    "file": "./client/templates/sign.html",
    "line": 174,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "video-games": {
    "key": "video-games",
    "text": "Video games",
    "file": "./client/templates/sign.html",
    "line": 176,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "tv-shows": {
    "key": "tv-shows",
    "text": "T.V. Shows",
    "file": "./client/templates/sign.html",
    "line": 178,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "movies": {
    "key": "movies",
    "text": "Movies",
    "file": "./client/templates/sign.html",
    "line": 180,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "languages": {
    "key": "languages",
    "text": "Languages",
    "file": "./client/templates/sign.html",
    "line": 182,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "education": {
    "key": "education",
    "text": "Education",
    "file": "./client/templates/sign.html",
    "line": 184,
    "template": "login_form",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "teaching": {
    "key": "teaching",
    "text": "Teaching",
    "file": "./client/templates/sign.html",
    "line": 186,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "travel": {
    "key": "travel",
    "text": "Travel abroad",
    "file": "./client/templates/sign.html",
    "line": 190,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "description": {
    "key": "description",
    "text": "Description",
    "file": "./client/templates/group.html",
    "line": 127,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "allowed_langs": {
    "key": "allowed_langs",
    "text": "Allowed Languages",
    "file": "./client/templates/group.html",
    "line": 130,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "select_one": {
    "key": "select_one",
    "text": "Select One",
    "file": "./client/templates/group.html",
    "line": 133,
    "template": "group_chat_finder",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "default-group-invite-message": {
    "key": "default-group-invite-message",
    "text": "Hi, check out this group, I think you\\",
    "file": "./client/templates/group.html",
    "line": 151,
    "template": "group_chat_finder",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "home": {
    "key": "home",
    "text": "Home",
    "file": "./client/templates/layout.html",
    "line": 66,
    "template": "title",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "blog": {
    "key": "blog",
    "text": "BLOG",
    "file": "./client/templates/layout.html",
    "line": 67,
    "template": "title",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "chat": {
    "key": "chat",
    "text": "CHAT",
    "file": "./client/templates/layout.html",
    "line": 68,
    "template": "title",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "notes": {
    "key": "notes",
    "text": "NOTES",
    "file": "./client/templates/layout.html",
    "line": 69,
    "template": "title",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "under-construction": {
    "key": "under-construction",
    "text": "Under development",
    "file": "./client/templates/layout.html",
    "line": 71,
    "template": "title",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "enter": {
    "key": "enter",
    "text": "Enter",
    "file": "./client/templates/sign.html",
    "line": 61,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "reset_password": {
    "key": "reset_password",
    "text": "Reset my password",
    "file": "./client/templates/sign.html",
    "line": 19,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "create_user": {
    "key": "create_user",
    "text": "Create user",
    "file": "./client/templates/sign.html",
    "line": 20,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "reset-passwd": {
    "key": "reset-passwd",
    "text": "Reset my password",
    "file": "./client/templates/user.html",
    "line": 503,
    "template": "user-profile",
    "ctime": 1404915874641,
    "mtime": 1404916320377
  },
  "reset": {
    "key": "reset",
    "text": "Reset",
    "file": "./client/templates/sign.html",
    "line": 44,
    "template": "login_form",
    "ctime": 1404915874641,
    "mtime": 1404915874641
  },
  "sing_up": {
    "key": "sing_up",
    "text": "Sign up",
    "file": "./client/templates/sign.html",
    "line": 218,
    "template": "login_form",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "basic-info": {
    "key": "basic-info",
    "text": "Basic Information",
    "file": "./client/templates/sign.html",
    "line": 91,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "name": {
    "key": "name",
    "text": "Name",
    "file": "./client/templates/sign.html",
    "line": 94,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "lname": {
    "key": "lname",
    "text": "Last name",
    "file": "./client/templates/sign.html",
    "line": 98,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "birthday": {
    "key": "birthday",
    "text": "Birthday",
    "file": "./client/templates/user.html",
    "line": 421,
    "template": "user-profile",
    "ctime": 1398734903623,
    "mtime": 1398734903623
  },
  "masculine": {
    "key": "masculine",
    "text": "Masculine",
    "file": "./client/templates/sign.html",
    "line": 106,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "feminine": {
    "key": "feminine",
    "text": "Feminine",
    "file": "./client/templates/sign.html",
    "line": 108,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "country": {
    "key": "country",
    "text": "Country",
    "file": "./client/templates/user.html",
    "line": 419,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "city": {
    "key": "city",
    "text": "City",
    "file": "./client/templates/user.html",
    "line": 418,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "native-lang": {
    "key": "native-lang",
    "text": "Native Language",
    "file": "./client/templates/sign.html",
    "line": 120,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "select": {
    "key": "select",
    "text": "Select One",
    "file": "./client/js/user.js",
    "line": 251,
    "func": "function(e,t)",
    "ctime": 1398734903623,
    "mtime": 1404915874642
  },
  "languages-information": {
    "key": "languages-information",
    "text": "Languages informations",
    "file": "./client/templates/sign.html",
    "line": 132,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "know-language": {
    "key": "know-language",
    "text": "I'm fluent in",
    "file": "./client/templates/user.html",
    "line": 437,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "add-more-languages": {
    "key": "add-more-languages",
    "text": "Add more lnaguages",
    "file": "./client/templates/user.html",
    "line": 455,
    "template": "user-profile",
    "ctime": 1398734903623,
    "mtime": 1398734903623
  },
  "learning-language": {
    "key": "learning-language",
    "text": "I'm learning",
    "file": "./client/templates/sign.html",
    "line": 151,
    "template": "login_form",
    "ctime": 1398735089360,
    "mtime": 1404915874642
  },
  "interests": {
    "key": "interests",
    "text": "Interests",
    "file": "./client/templates/user.html",
    "line": 390,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "make-friends": {
    "key": "make-friends",
    "text": "Make new friends",
    "file": "./client/templates/sign.html",
    "line": 188,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "login-info": {
    "key": "login-info",
    "text": "Login information",
    "file": "./client/templates/sign.html",
    "line": 196,
    "template": "login_form",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "passwd": {
    "key": "passwd",
    "text": "Password",
    "file": "./client/templates/sign.html",
    "line": 204,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1404915874642
  },
  "passwd_check": {
    "key": "passwd_check",
    "text": "Password check",
    "file": "./client/templates/sign.html",
    "line": 208,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "close": {
    "key": "close",
    "text": "Close",
    "file": "./client/templates/sign.html",
    "line": 217,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "profile": {
    "key": "profile",
    "text": "Profile",
    "file": "./client/templates/user.html",
    "line": 29,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "notification": {
    "key": "notification",
    "text": "Notifications",
    "file": "./client/templates/user.html",
    "line": 33,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "log_out": {
    "key": "log_out",
    "text": "Log out",
    "file": "./client/templates/user.html",
    "line": 35,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "sign_in": {
    "key": "sign_in",
    "text": "Sign in",
    "file": "./client/templates/user.html",
    "line": 42,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1396799101552
  },
  "friendship-request": {
    "key": "friendship-request",
    "text": "User friendship request",
    "file": "./client/templates/user.html",
    "line": 79,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "acept": {
    "key": "acept",
    "text": "Acept",
    "file": "./client/templates/user.html",
    "line": 84,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "deny": {
    "key": "deny",
    "text": "Deny",
    "file": "./client/templates/user.html",
    "line": 85,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "participation-request": {
    "key": "participation-request",
    "text": "Group participation request",
    "file": "./client/templates/user.html",
    "line": 89,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "invitation-request": {
    "key": "invitation-request",
    "text": "Group participation invitation request",
    "file": "./client/templates/user.html",
    "line": 99,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "find-user": {
    "key": "find-user",
    "text": "Find user",
    "file": "./client/templates/user.html",
    "line": 123,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "user-name": {
    "key": "user-name",
    "text": "User name",
    "file": "./client/templates/user.html",
    "line": 131,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "invite-friendship": {
    "key": "invite-friendship",
    "text": "Sen a friendship request",
    "file": "./client/templates/user.html",
    "line": 155,
    "template": "user-profile",
    "ctime": 1398734903624,
    "mtime": 1398734903624
  },
  "user-info": {
    "key": "user-info",
    "text": "User basic info",
    "file": "./client/templates/user.html",
    "line": 410,
    "template": "user-profile",
    "ctime": 1398734903624,
    "mtime": 1398734903624
  },
  "user-languages": {
    "key": "user-languages",
    "text": "User languages info",
    "file": "./client/templates/user.html",
    "line": 426,
    "template": "user-profile",
    "ctime": 1398734903624,
    "mtime": 1398734903624
  },
  "know": {
    "key": "know",
    "text": "Know",
    "file": "./client/templates/user.html",
    "line": 374,
    "template": "user-profile",
    "ctime": 1398734903624,
    "mtime": 1398734903624
  },
  "learning": {
    "key": "learning",
    "text": "Learning",
    "file": "./client/templates/user.html",
    "line": 381,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1398734903623
  },
  "add-user": {
    "key": "add-user",
    "text": "Add contact",
    "file": "./client/templates/user.html",
    "line": 212,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "friend": {
    "key": "friend",
    "text": "Friend",
    "file": "./client/templates/user.html",
    "line": 308,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "contact": {
    "key": "contact",
    "text": "contact",
    "file": "./client/templates/user.html",
    "line": 310,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "teacher": {
    "key": "teacher",
    "text": "Teacher",
    "file": "./client/templates/user.html",
    "line": 312,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398734903624
  },
  "student": {
    "key": "student",
    "text": "Student",
    "file": "./client/templates/user.html",
    "line": 314,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "info": {
    "key": "info",
    "text": "Info",
    "file": "./client/templates/user.html",
    "line": 261,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "security": {
    "key": "security",
    "text": "Security",
    "file": "./client/templates/user.html",
    "line": 262,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "friends": {
    "key": "friends",
    "text": "Friends",
    "file": "./client/templates/user.html",
    "line": 263,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "user": {
    "key": "user",
    "text": "User",
    "file": "./client/templates/user.html",
    "line": 305,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "groups": {
    "key": "groups",
    "text": "Groups",
    "file": "./client/templates/user.html",
    "line": 327,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "invite_to_group": {
    "key": "invite_to_group",
    "text": "Invite the user for this group",
    "file": "./client/templates/user.html",
    "line": 332,
    "template": "user-profile",
    "ctime": 1398707194597,
    "mtime": 1398707194597
  },
  "report": {
    "key": "report",
    "text": "Report",
    "file": "./client/templates/user.html",
    "line": 343,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "reason": {
    "key": "reason",
    "text": "Reason",
    "file": "./client/templates/user.html",
    "line": 344,
    "template": "user-profile",
    "ctime": 1398341669193,
    "mtime": 1398341669193
  },
  "edit-info": {
    "key": "edit-info",
    "text": "Edit info",
    "file": "./client/templates/user.html",
    "line": 354,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "native": {
    "key": "native",
    "text": "Native",
    "file": "./client/templates/user.html",
    "line": 376,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1404915874642
  },
  "fluent": {
    "key": "fluent",
    "text": "Fluent",
    "file": "./client/templates/user.html",
    "line": 378,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "user-description": {
    "key": "user-description",
    "text": "User description",
    "file": "./client/templates/user.html",
    "line": 470,
    "template": "user-profile",
    "ctime": 1398734903624,
    "mtime": 1398734903624
  },
  "learning-languages": {
    "key": "learning-languages",
    "text": "I'm learning",
    "file": "./client/templates/user.html",
    "line": 453,
    "template": "user-profile",
    "mtime": 1406039292447,
    "ctime": 1406039292447
  },
  "email-info": {
    "key": "email-info",
    "text": "Enter your account email",
    "file": "./client/templates/user.html",
    "line": 505,
    "template": "user-profile",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "akan": {
    "key": "akan",
    "text": "Akan",
    "file": "./client/js/client.js",
    "line": 36,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "amharic": {
    "key": "amharic",
    "text": "Amharic",
    "file": "./client/js/client.js",
    "line": 37,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "arabic": {
    "key": "arabic",
    "text": "Arabic",
    "file": "./client/js/client.js",
    "line": 38,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "assamese": {
    "key": "assamese",
    "text": "Assamese",
    "file": "./client/js/client.js",
    "line": 39,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "awadhi": {
    "key": "awadhi",
    "text": "Awadhi",
    "file": "./client/js/client.js",
    "line": 40,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "azerbaijani": {
    "key": "azerbaijani",
    "text": "Azerbaijani",
    "file": "./client/js/client.js",
    "line": 41,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "balochi": {
    "key": "balochi",
    "text": "Balochi",
    "file": "./client/js/client.js",
    "line": 42,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "belarusian": {
    "key": "belarusian",
    "text": "Belarusian",
    "file": "./client/js/client.js",
    "line": 43,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "bengali": {
    "key": "bengali",
    "text": "Bengali",
    "file": "./client/js/client.js",
    "line": 44,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "bhojpuri": {
    "key": "bhojpuri",
    "text": "Bhojpuri",
    "file": "./client/js/client.js",
    "line": 45,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "burmese": {
    "key": "burmese",
    "text": "Burmese",
    "file": "./client/js/client.js",
    "line": 46,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "cantonese": {
    "key": "cantonese",
    "text": "Cantonese",
    "file": "./client/js/client.js",
    "line": 47,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "cebuano": {
    "key": "cebuano",
    "text": "Cebuano",
    "file": "./client/js/client.js",
    "line": 48,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "chewa": {
    "key": "chewa",
    "text": "Chewa",
    "file": "./client/js/client.js",
    "line": 49,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "chhattisgarhi": {
    "key": "chhattisgarhi",
    "text": "Chhattisgarhi",
    "file": "./client/js/client.js",
    "line": 50,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "chittagonian": {
    "key": "chittagonian",
    "text": "Chittagonian",
    "file": "./client/js/client.js",
    "line": 51,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "czech": {
    "key": "czech",
    "text": "Czech",
    "file": "./client/js/client.js",
    "line": 52,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "deccan": {
    "key": "deccan",
    "text": "Deccan",
    "file": "./client/js/client.js",
    "line": 53,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "dhundhari": {
    "key": "dhundhari",
    "text": "Dhundhari",
    "file": "./client/js/client.js",
    "line": 54,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "dutch": {
    "key": "dutch",
    "text": "Dutch",
    "file": "./client/js/client.js",
    "line": 55,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "english": {
    "key": "english",
    "text": "English",
    "file": "./client/js/client.js",
    "line": 56,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "french": {
    "key": "french",
    "text": "French",
    "file": "./client/js/client.js",
    "line": 57,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "fula": {
    "key": "fula",
    "text": "Fula",
    "file": "./client/js/client.js",
    "line": 58,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "gan": {
    "key": "gan",
    "text": "Gan",
    "file": "./client/js/client.js",
    "line": 59,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "german": {
    "key": "german",
    "text": "German",
    "file": "./client/js/client.js",
    "line": 60,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "greek": {
    "key": "greek",
    "text": "Greek",
    "file": "./client/js/client.js",
    "line": 61,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "gujarati": {
    "key": "gujarati",
    "text": "Gujarati",
    "file": "./client/js/client.js",
    "line": 62,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "haitian creole": {
    "key": "haitian creole",
    "text": "Haitian Creole",
    "file": "./client/js/client.js",
    "line": 63,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hakka": {
    "key": "hakka",
    "text": "Hakka",
    "file": "./client/js/client.js",
    "line": 64,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "haryanvi": {
    "key": "haryanvi",
    "text": "Haryanvi",
    "file": "./client/js/client.js",
    "line": 65,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hausa": {
    "key": "hausa",
    "text": "Hausa",
    "file": "./client/js/client.js",
    "line": 66,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hiligaynon": {
    "key": "hiligaynon",
    "text": "Hiligaynon",
    "file": "./client/js/client.js",
    "line": 67,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hindi": {
    "key": "hindi",
    "text": "Hindi",
    "file": "./client/js/client.js",
    "line": 68,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hmong": {
    "key": "hmong",
    "text": "Hmong",
    "file": "./client/js/client.js",
    "line": 69,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "hungarian": {
    "key": "hungarian",
    "text": "Hungarian",
    "file": "./client/js/client.js",
    "line": 70,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "igbo": {
    "key": "igbo",
    "text": "Igbo",
    "file": "./client/js/client.js",
    "line": 71,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "ilokano": {
    "key": "ilokano",
    "text": "Ilokano",
    "file": "./client/js/client.js",
    "line": 72,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "italian": {
    "key": "italian",
    "text": "Italian",
    "file": "./client/js/client.js",
    "line": 73,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "japanese": {
    "key": "japanese",
    "text": "Japanese",
    "file": "./client/js/client.js",
    "line": 74,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "javanese": {
    "key": "javanese",
    "text": "Javanese",
    "file": "./client/js/client.js",
    "line": 75,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "jin": {
    "key": "jin",
    "text": "Jin",
    "file": "./client/js/client.js",
    "line": 76,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "kannada": {
    "key": "kannada",
    "text": "Kannada",
    "file": "./client/js/client.js",
    "line": 77,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "kazakh": {
    "key": "kazakh",
    "text": "Kazakh",
    "file": "./client/js/client.js",
    "line": 78,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "khmer": {
    "key": "khmer",
    "text": "Khmer",
    "file": "./client/js/client.js",
    "line": 79,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "kinyarwanda": {
    "key": "kinyarwanda",
    "text": "Kinyarwanda",
    "file": "./client/js/client.js",
    "line": 80,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "kirundi": {
    "key": "kirundi",
    "text": "Kirundi",
    "file": "./client/js/client.js",
    "line": 81,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "konkani": {
    "key": "konkani",
    "text": "Konkani",
    "file": "./client/js/client.js",
    "line": 82,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "korean": {
    "key": "korean",
    "text": "Korean",
    "file": "./client/js/client.js",
    "line": 83,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "kurdish": {
    "key": "kurdish",
    "text": "Kurdish",
    "file": "./client/js/client.js",
    "line": 84,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "madurese": {
    "key": "madurese",
    "text": "Madurese",
    "file": "./client/js/client.js",
    "line": 85,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "magahi": {
    "key": "magahi",
    "text": "Magahi",
    "file": "./client/js/client.js",
    "line": 86,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "maithili": {
    "key": "maithili",
    "text": "Maithili",
    "file": "./client/js/client.js",
    "line": 87,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "malagasy": {
    "key": "malagasy",
    "text": "Malagasy",
    "file": "./client/js/client.js",
    "line": 88,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "malay/indonesian": {
    "key": "malay/indonesian",
    "text": "Malay/Indonesian",
    "file": "./client/js/client.js",
    "line": 89,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "malayalam": {
    "key": "malayalam",
    "text": "Malayalam",
    "file": "./client/js/client.js",
    "line": 90,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "mandarin": {
    "key": "mandarin",
    "text": "Mandarin",
    "file": "./client/js/client.js",
    "line": 91,
    "func": "function()",
    "ctime": 1404915874642,
    "mtime": 1404915874642
  },
  "marathi": {
    "key": "marathi",
    "text": "Marathi",
    "file": "./client/js/client.js",
    "line": 92,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "marwari": {
    "key": "marwari",
    "text": "Marwari",
    "file": "./client/js/client.js",
    "line": 93,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "min bei": {
    "key": "min bei",
    "text": "Min Bei",
    "file": "./client/js/client.js",
    "line": 94,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "min dong": {
    "key": "min dong",
    "text": "Min Dong",
    "file": "./client/js/client.js",
    "line": 95,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "min nan": {
    "key": "min nan",
    "text": "Min Nan",
    "file": "./client/js/client.js",
    "line": 96,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "mossi": {
    "key": "mossi",
    "text": "Mossi",
    "file": "./client/js/client.js",
    "line": 97,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "nepali": {
    "key": "nepali",
    "text": "Nepali",
    "file": "./client/js/client.js",
    "line": 98,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "oriya": {
    "key": "oriya",
    "text": "Oriya",
    "file": "./client/js/client.js",
    "line": 99,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "oromo": {
    "key": "oromo",
    "text": "Oromo",
    "file": "./client/js/client.js",
    "line": 100,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "pashto": {
    "key": "pashto",
    "text": "Pashto",
    "file": "./client/js/client.js",
    "line": 101,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "persian": {
    "key": "persian",
    "text": "Persian",
    "file": "./client/js/client.js",
    "line": 102,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "polish": {
    "key": "polish",
    "text": "Polish",
    "file": "./client/js/client.js",
    "line": 103,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "portuguese": {
    "key": "portuguese",
    "text": "Portuguese",
    "file": "./client/js/client.js",
    "line": 104,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "punjabi": {
    "key": "punjabi",
    "text": "Punjabi",
    "file": "./client/js/client.js",
    "line": 105,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "quechua": {
    "key": "quechua",
    "text": "Quechua",
    "file": "./client/js/client.js",
    "line": 106,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "romanian": {
    "key": "romanian",
    "text": "Romanian",
    "file": "./client/js/client.js",
    "line": 107,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "russian": {
    "key": "russian",
    "text": "Russian",
    "file": "./client/js/client.js",
    "line": 108,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "saraiki": {
    "key": "saraiki",
    "text": "Saraiki",
    "file": "./client/js/client.js",
    "line": 109,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "serbo-croatian": {
    "key": "serbo-croatian",
    "text": "Serbo-Croatian",
    "file": "./client/js/client.js",
    "line": 110,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "shona": {
    "key": "shona",
    "text": "Shona",
    "file": "./client/js/client.js",
    "line": 111,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "sindhi": {
    "key": "sindhi",
    "text": "Sindhi",
    "file": "./client/js/client.js",
    "line": 112,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "sinhalese": {
    "key": "sinhalese",
    "text": "Sinhalese",
    "file": "./client/js/client.js",
    "line": 113,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "somali": {
    "key": "somali",
    "text": "Somali",
    "file": "./client/js/client.js",
    "line": 114,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "spanish": {
    "key": "spanish",
    "text": "Spanish",
    "file": "./client/js/client.js",
    "line": 115,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "sundanese": {
    "key": "sundanese",
    "text": "Sundanese",
    "file": "./client/js/client.js",
    "line": 116,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "swahili": {
    "key": "swahili",
    "text": "Swahili",
    "file": "./client/js/client.js",
    "line": 117,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "swedish": {
    "key": "swedish",
    "text": "Swedish",
    "file": "./client/js/client.js",
    "line": 118,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "sylheti": {
    "key": "sylheti",
    "text": "Sylheti",
    "file": "./client/js/client.js",
    "line": 119,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "tagalog": {
    "key": "tagalog",
    "text": "Tagalog",
    "file": "./client/js/client.js",
    "line": 120,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "tamil": {
    "key": "tamil",
    "text": "Tamil",
    "file": "./client/js/client.js",
    "line": 121,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "telugu": {
    "key": "telugu",
    "text": "Telugu",
    "file": "./client/js/client.js",
    "line": 122,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "thai": {
    "key": "thai",
    "text": "Thai",
    "file": "./client/js/client.js",
    "line": 123,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "turkish": {
    "key": "turkish",
    "text": "Turkish",
    "file": "./client/js/client.js",
    "line": 124,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "ukrainian": {
    "key": "ukrainian",
    "text": "Ukrainian",
    "file": "./client/js/client.js",
    "line": 125,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "urdu": {
    "key": "urdu",
    "text": "Urdu",
    "file": "./client/js/client.js",
    "line": 126,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "uyghur": {
    "key": "uyghur",
    "text": "Uyghur",
    "file": "./client/js/client.js",
    "line": 127,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "uzbek": {
    "key": "uzbek",
    "text": "Uzbek",
    "file": "./client/js/client.js",
    "line": 128,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "vietnamese": {
    "key": "vietnamese",
    "text": "Vietnamese",
    "file": "./client/js/client.js",
    "line": 129,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "wu": {
    "key": "wu",
    "text": "Wu",
    "file": "./client/js/client.js",
    "line": 130,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "xhosa": {
    "key": "xhosa",
    "text": "Xhosa",
    "file": "./client/js/client.js",
    "line": 131,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "xiang": {
    "key": "xiang",
    "text": "Xiang",
    "file": "./client/js/client.js",
    "line": 132,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "yoruba": {
    "key": "yoruba",
    "text": "Yoruba",
    "file": "./client/js/client.js",
    "line": 133,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "zhuang": {
    "key": "zhuang",
    "text": "Zhuang",
    "file": "./client/js/client.js",
    "line": 134,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "zulu": {
    "key": "zulu",
    "text": "Zulu",
    "file": "./client/js/client.js",
    "line": 135,
    "func": "function()",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "online": {
    "key": "online",
    "text": "Online",
    "file": "./client/js/user.js",
    "line": 73,
    "func": "function(e,t)",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "away": {
    "key": "away",
    "text": "Away",
    "file": "./client/js/user.js",
    "line": 74,
    "func": "function(e,t)",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "busy": {
    "key": "busy",
    "text": "Busy",
    "file": "./client/js/user.js",
    "line": 75,
    "func": "function(e,t)",
    "ctime": 1404915874643,
    "mtime": 1404915874643
  },
  "group_genre": {
    "key": "group_genre",
    "text": "Group Genre",
    "file": "./client/templates/chatroons.html",
    "line": 270,
    "template": "chatrooms",
    "ctime": 1398341669193,
    "mtime": 1406039292448,
    "removed": true
  },
  "forgot_passwd": {
    "key": "forgot_passwd",
    "text": "I forgot my password",
    "file": "./client/templates/sign.html",
    "line": 18,
    "template": "login_form",
    "ctime": 1396799101552,
    "mtime": 1406039292448,
    "removed": true
  },
  "messages": {
    "key": "messages",
    "text": "Messages",
    "file": "./client/templates/user.html",
    "line": 30,
    "template": "user-profile",
    "ctime": 1396799101552,
    "mtime": 1406039292448,
    "removed": true
  },
  "sign_up": {
    "key": "sign_up",
    "text": "Sign up",
    "file": "./client/templates/layout.html",
    "line": 148,
    "template": "title",
    "ctime": 1396799101552,
    "mtime": 1406039292448,
    "removed": true
  },
  "public_roons": {
    "key": "public_roons",
    "text": "Public chat roons",
    "file": "./client/templates/chatroons.html",
    "line": 147,
    "template": "chatroons",
    "ctime": 1396790754391,
    "mtime": 1406039292448,
    "removed": true
  }
}, 
{
  "extractedAt": 1406039292448,
  "updatedAt": 1406039292448
});
